import WebSocket from 'ws';
import Bot from 'ubot';

export default (configuration) => ({

  bot: null,
  conversation: null,

  connect: (context) => async function(ws, req) {

    const [ middlewares, recognizers ] = context.middlewares;
    this.conversation = this.bot.start(this.bot);
    const result = await this.bot.sequentialLoad(context, middlewares);

    ws.on('message',
      this.message(
        ws,
        { ...context, ...result },
        recognizers
      ).bind(this)
    );
  },

  message: (ws, context, recognizers) => async function(response) {

    const message = JSON.parse(response);
    const {
      WIT_BASE_URL,
      WIT_TOKEN,
      WIT_VERSION
    } = configuration;

    const incoming = await this.bot.sequentialLoad({
      ...context,
      message,
      WIT_BASE_URL,
      WIT_TOKEN,
      WIT_VERSION
    }, recognizers);

    this.conversation.answer({
      text: message.text,
      ...context,
      ...incoming,
      sendMessage: context.send(ws, { ...context, ...incoming, message })
    })
  },

  init: async function (context) {

    const wss = new WebSocket.Server({ port: configuration.PORT })
    this.bot = Bot({});
    this.bot.registerAll(context.dialogs);
    wss.on('connection', this.connect(context).bind(this));

  }

})
