export default {
  id: 'none',
  triggers: [ 'intent.none' ],
  steps: [
    async (context) => {
      await context.send('no te he entendido');
      return await context.endDialog();
    }
  ]
}
