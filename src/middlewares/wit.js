const request = require('request-promise-native');

module.exports = async ({
  message,
  WIT_BASE_URL,
  WIT_VERSION,
  WIT_TOKEN
}) => {

  const { text } = typeof message === 'string' ? { text: message } : message;

  const options = {
    uri: `${WIT_BASE_URL}?v=${WIT_VERSION}&q=${message.text}`,
    headers: {
        Authorization: `Bearer ${WIT_TOKEN}` ,
    },
    json: true
};

  const response = await request(options);

  const { entities, _text } = response;

  const intent = entities.intent ? getTopResult(entities.intent) : {
    value: 'intent.none',
    confidence: 1
  };

  return {
    intents: entities.intent,
    intent,
    topIntent: intent.value,
    entities: getEntities(entities)
  }
}

function findIntent(entities, _text) {
  return Object.entries(entities).map(
      ([key, value]) =>
        value.reduce(
          (acc, val) => ({
            ...acc,
            [val.value]: {
              type: key,
              confidence: val.confidence,
              entities: getEntities(entities)
            }
          }),
      {})
    );
}

function getEntities(entities) {
  return Object.keys(entities)
    .filter(key => key !== 'intent')
    .reduce((acc, key) => {
      const topEntity = getTopResult(entities[key]);
      const entity = {
        ...topEntity,
        type: `ent.${key}`
      }
      return [ ...acc, entity ];
    }, [])
}


function getTopResult(results = []) {
  const confidence = Math.max(results.map(({ confidence }) => confidence));
  return results.find(result => result.confidence === confidence);
}
