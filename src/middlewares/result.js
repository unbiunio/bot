module.exports = async ({
  dialogs,
  entities,
  intent,
  topIntent,
}) => {

  const { id } = dialogs.find(
    ({ triggers }) => {
      return triggers.includes(topIntent)
    }
  ) || {};

  return { entities, intent, did: id }
}
