import wit from './wit'
import result from './result'
import conversation from './conversation'

module.exports = {
  conversation: [

  ],
  incoming: [
    wit,
    result
  ]
}
