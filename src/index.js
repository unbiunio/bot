import 'dotenv/config';

import Bot from './bot';

import dialogs from './dialogs'
import middlewares from './middlewares';

const bot = Bot(process.env);

bot.init({
  dialogs,
  middlewares: [ middlewares.conversation, middlewares.incoming ],
  send: (ws, context) => (m) => {
    console.log('message context', m)
    ws.send(
      JSON.stringify({
        ...m,
        time: Date.now()
    }))
  }
});
