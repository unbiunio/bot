const path = require("path");

module.exports = {
  entry: './src/index.js',
  mode: 'development',
  plugins: [],
  target: 'node',
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'lib'),
    library: 'bot',
    libraryTarget: 'umd',
    umdNamedDefine: true
  }
};
